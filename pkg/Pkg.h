/*
 * Pkg.h
 *
 *  Created on: 05/03/2015
 *      Author: thebeast
 */

#include <iostream>
#include <cstring>
#include <getopt.h>

#include "../libpkg/LibPkg.h"


#ifndef PKG_PKG_H_
#define PKG_PKG_H_

typedef enum ACTIONS
{
	NONE,
	INSTALL,
	REMOVE,
	UPDATE,
	CONFIG,
	PACKAGE,
	EXTRACT,
	NEWDB,
	QUERY,
	HELP,
} action_t;

#define PROGNAME			"pkg"
#define SHORT_OPTS			"irucpenq:hd:vs"
#define INSTALL_LONG		"install"
#define INSTALL_SHORT		'i'
#define REMOVE_LONG			"remove"
#define REMOVE_SHORT		'r'
#define UPDATE_LONG			"update"
#define UPDATE_SHORT		'u'
#define CONFIG_LONG			"config"
#define CONFIG_SHORT		'c'
#define PACKAGE_LONG		"package"
#define PACKAGE_SHORT		'p'
#define EXTRACT_LONG		"unpackage"
#define EXTRACT_SHORT		'e'
#define NEWDB_LONG			"newdb"
#define NEWDB_SHORT			'n'
#define QUERY_LONG			"query"
#define QUERY_SHORT			'q'
#define HELP_LONG			"help"
#define HELP_SHORT			'h'
#define DESTROOT_LONG		"destroot"
#define DESTROOT_SHORT		'd'
#define VERBOSE_LONG		"verbose"
#define VERBOSE_SHORT		'v'
#define SILENT_LONG			"silent"
#define SILENT_SHORT		's'
#define NOSCRIPT_LONG		"noscript"
#define NOSCRIPT_SHORT		'1'
#define NOTRIGGERS_LONG		"notriggers"
#define NOTRIGGERS_SHORT	'2'
#define NORUNDEPS_LONG		"norundes"
#define NORUNDEPS_SHORT		'3'
#define NOCONFLICTS_LONG	"noconflicts"
#define NOCONFLICTS_SHORT	'4'
#define NOCONFIG_LONG		"noconfig"
#define NOCONFIG_SHORT		'5'
#define NOHOLD_LONG			"nohold"
#define NOHOLD_SHORT		'6'


#endif /* PKG_PKG_H_ */
