/*
 * Pkg.cpp
 *
 *  Created on: 17/02/2015
 *      Author: miguel
 */

#include "Pkg.h"

#include <iostream>
#include <cstdlib>
#include <stdexcept>
#include <locale.h>

#include "../libpkg/LibPkg.h"
#include "RunTime.h"


int main(int argc, char** argv)
{
    bindtextdomain(argv[0], "/usr/share/locale");
    textdomain(argv[0]);

	Runtime rt;

	if( !rt.foundSystemArch() )
	{
		std::cout << _("Unable to find system architecture\n");
		return EXIT_FAILURE;
	}

	if( !rt.foundPackagesArch() )
	{
		std::cout << _("Unable to find packages architecture\n");
		return EXIT_FAILURE;
	}

	if( !rt.foundValidArchs() )
	{
		std::cout << _("Unable to find supported packages architectures\n");
		return EXIT_FAILURE;
	}

	try
	{
		rt.parseArguments(argc, argv, rt);
	}
	catch(const std::runtime_error& e)
	{
		std::cout << e.what();
	}

	try
	{
		rt.executeAction(rt);
	}
	catch(const std::runtime_error& e)
	{
		std::cout << e.what();
	}

	return EXIT_SUCCESS;
}
