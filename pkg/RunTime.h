/*
 * Runtime.h
 *
 *  Created on: 15/03/2015
 *      Author: thebeast
 */

#include <cstring>
#include <vector>

#include "../libpkg/LibPkg.h"
#include "../libpkg/System.h"


#ifndef PKG_RUNTIME_H_
#define PKG_RUNTIME_H_

class Runtime : public System
{
private :
	action_t action;
	std::string destroot;
	std::string fields;
    bolean_t verbose;
    bolean_t silent;
    bolean_t scripts;
    bolean_t triggers;
    bolean_t rundeps;
    bolean_t conflicts;
    bolean_t config;
    bolean_t hold;
    std::vector<std::string> pkgv;

    ushort_t executeInstall(Runtime rt);
    ushort_t executeRemove(Runtime rt);
    ushort_t executeUpdate(Runtime rt);
    ushort_t executeConfig(Runtime rt);
    ushort_t executePackage(Runtime rt);
    ushort_t executeExtract(Runtime rt);
    ushort_t executeNewDb(Runtime rt);
    ushort_t executeQuery(Runtime rt);
    void executeHelp(void);

public :
	Runtime();

	ushort_t parseArguments(int argc, char** argv, Runtime &rt);
	ushort_t executeAction(Runtime rt);
};

#endif /* PKG_RUNTIME_H_ */
