/*
 * Runtime.cpp
 *
 *  Created on: 05/03/2015
 *      Author: thebeast
 */


#include <cctype>
#include <unistd.h>
#include <cstdio>
#include <getopt.h>

#include "Pkg.h"
#include "RunTime.h"
#include "../libpkg/UnixHandlers.h"

Runtime::Runtime()
{
	action = NONE;
	destroot = "/";
	verbose = FALSE;
	silent = FALSE;
	scripts = TRUE;
	triggers = TRUE;
	rundeps = TRUE;
	conflicts = TRUE;
	config = TRUE;
	hold = TRUE;
}

ushort_t Runtime::parseArguments(int argc, char** argv, Runtime &rt)
{

	int next_opt;
	const char* short_opts = SHORT_OPTS;
	const struct option long_opts[] =
	{
		{ INSTALL_LONG,		NOREQUIRED,		NULL,		INSTALL_SHORT     },
		{ REMOVE_LONG,		NOREQUIRED,		NULL,		REMOVE_SHORT      },
		{ UPDATE_LONG,		NOREQUIRED,		NULL,		UPDATE_SHORT      },
		{ CONFIG_LONG,		NOREQUIRED,		NULL,		CONFIG_SHORT      },
		{ PACKAGE_LONG,		NOREQUIRED,		NULL,		PACKAGE_SHORT     },
		{ EXTRACT_LONG,		NOREQUIRED,		NULL,		EXTRACT_SHORT     },
		{ NEWDB_LONG,		NOREQUIRED,		NULL,		NEWDB_SHORT       },
		{ QUERY_LONG,		REQUIRED,		NULL,		QUERY_SHORT       },
		{ HELP_LONG,		NOREQUIRED,		NULL,		HELP_SHORT        },
		{ DESTROOT_LONG,	REQUIRED,		NULL,		DESTROOT_SHORT    },
		{ VERBOSE_LONG,		NOREQUIRED,		NULL,		VERBOSE_SHORT     },
		{ SILENT_LONG,		NOREQUIRED,		NULL,		SILENT_SHORT      },
		{ NOSCRIPT_LONG,	NOREQUIRED,		NULL,		NOSCRIPT_SHORT    },
		{ NOTRIGGERS_LONG,	NOREQUIRED,		NULL,		NOTRIGGERS_SHORT  },
		{ NORUNDEPS_LONG,	NOREQUIRED,		NULL,		NORUNDEPS_SHORT   },
		{ NOCONFLICTS_LONG,	NOREQUIRED,		NULL,		NOCONFLICTS_SHORT },
		{ NOCONFIG_LONG,	NOREQUIRED,		NULL,		NOCONFIG_SHORT    },
		{ NOHOLD_LONG,		NOREQUIRED,		NULL,		NOHOLD_SHORT      },
		{ NULL,				NOREQUIRED,		NULL,		FALSE             },
	};

	opterr = 0;

	while(1)
	{
		next_opt = getopt_long(argc, argv, short_opts, long_opts, NULL);
		if(next_opt == -1)
			break;

		switch(next_opt)
		{
			case INSTALL_SHORT :
				rt.action = INSTALL;
				break;
			case REMOVE_SHORT :
				rt.action = REMOVE;
				break;
			case UPDATE_SHORT :
				rt.action = UPDATE;
				break;
			case CONFIG_SHORT :
				rt.action = CONFIG;
				break;
			case PACKAGE_SHORT :
				rt.action = PACKAGE;
				break;
			case EXTRACT_SHORT :
				rt.action = EXTRACT;
				break;
			case NEWDB_SHORT :
				rt.action = NEWDB;
				break;
			case QUERY_SHORT :
				rt.action = QUERY;
				rt.fields = (std::string)optarg;
				break;
			case HELP_SHORT :
				rt.action = HELP;
				break;
			case DESTROOT_SHORT :
				rt.destroot= (std::string)optarg;
				break;
			case VERBOSE_SHORT :
				rt.silent = FALSE;
				rt.verbose = TRUE;
				break;
			case SILENT_SHORT :
				rt.verbose = FALSE;
				rt.silent = TRUE;
				break;
			case NOSCRIPT_SHORT :
				rt.scripts = FALSE;
				break;
			case NOTRIGGERS_SHORT :
				rt.triggers = FALSE;
				break;
			case NORUNDEPS_SHORT :
				rt.rundeps = FALSE;
				break;
			case NOCONFLICTS_SHORT :
				rt.conflicts = FALSE;
				break;
			case NOCONFIG_SHORT :
				rt.config = FALSE;
				break;
			case NOHOLD_SHORT :
				rt.hold = FALSE;
				break;
			case '?' :
			{
				switch(optopt)
				{
					case DESTROOT_SHORT :
						std::cout << "-" << (char)optopt << _(" option require an argument to set them as new root\n");
						break;
					case QUERY_SHORT :
						std::cout << "-" << (char)optopt << _(" option require an argument to set them as query mode\n");
						break;
					default :
						if(isprint(optopt))
						{
							std::cout << "-" << (char)optopt << _(" Unknow option\n");
						} else {
							std::cout << optopt << _(" Unknow charset\n");
						}
				}
			}
		}
	}


	if ( (argc - optind) > 0 )
	{
		int i=optind;
		do
		{
			rt.pkgv.push_back((std::string)argv[i]);
			i++;
		} while(i < argc);
	}

	return RETURN_SUCCESS;
}

ushort_t Runtime::executeInstall(Runtime rt)
{

	return RETURN_SUCCESS;
}

ushort_t Runtime::executeRemove(Runtime rt)
{

	return RETURN_SUCCESS;
}

ushort_t Runtime::executeUpdate(Runtime rt)
{

	return RETURN_SUCCESS;
}

ushort_t Runtime::executeConfig(Runtime rt)
{

	return RETURN_SUCCESS;
}

ushort_t Runtime::executePackage(Runtime rt)
{

	return RETURN_SUCCESS;
}

ushort_t Runtime::executeExtract(Runtime rt)
{

	return RETURN_SUCCESS;
}

ushort_t Runtime::executeNewDb(Runtime rt)
{
	UnixHandler hdl(rt.pkgv[0]);

	std::cout << hdl.fullPath;

	return RETURN_SUCCESS;
}

ushort_t Runtime::executeQuery(Runtime rt)
{

	return RETURN_SUCCESS;
}

void Runtime::executeHelp(void)
{
	printf(_(
			"\n %s <action> <arguments> <packages>\n"
			"\n"
			"Actions :\n"
			" -%c, --%s\t Install provided packages as arguments\n"
			" -%c, --%s\t Remove provided packages as arguments\n"
			" -%c, --%s\t Update installed package whit one new provide as -w argument\n"
			" -%c, --%s\t Configure or reconfigure installed packages\n"
			" -%c, --%s\t Create a package file with given directory\n"
			" -%c, --%s Extract package content to a given directory\n"
			" -%c, --%s\t Create a new packages database installation\n"
			" -%c, --%s\t Run query to a local packages database\n"
			" -%c, --%s\t Show this help and exit\n"
			"\n\n"
			"Arguments :\n"
			" -%c, --%s\t Set destination root directory\n"
			" -%c, --%s\t Set verbose mode\n"
			" -%c, --%s\t Set silent mode\n"
			" --%s\t Disable install script execution\n"
			" --%s\t Disable triggers execution\n"
			" --%s\t Disable run depends check\n"
			" --%s\t Disable package conflicts check\n"
			" --%s\t Disable package configuration\n"
			" --%s\t Disable packages hold check\n"
			), \
	PROGNAME, \
	INSTALL_SHORT, \
	INSTALL_LONG, \
	REMOVE_SHORT, \
	REMOVE_LONG, \
	UPDATE_SHORT, \
	UPDATE_LONG, \
	CONFIG_SHORT, \
	CONFIG_LONG, \
	PACKAGE_SHORT, \
	PACKAGE_LONG, \
	EXTRACT_SHORT, \
	EXTRACT_LONG, \
	NEWDB_SHORT, \
	NEWDB_LONG, \
	QUERY_SHORT, \
	QUERY_LONG, \
	HELP_SHORT, \
	HELP_LONG, \
	DESTROOT_SHORT, \
	DESTROOT_LONG, \
	VERBOSE_SHORT, \
	VERBOSE_LONG, \
	SILENT_SHORT, \
	SILENT_LONG, \
	NOSCRIPT_LONG, \
	NOTRIGGERS_LONG, \
	NORUNDEPS_LONG, \
	NOCONFLICTS_LONG, \
	NOCONFIG_LONG, \
	NOHOLD_LONG \
	);
}

ushort_t Runtime::executeAction(Runtime rt)
{
	switch(rt.action)
	{
		case NONE :
			std::cout << _("No runtimen action was defined") << std::endl;
			return RETURN_FAILURE;
		case INSTALL :
			this->executeInstall(rt);
			break;
		case REMOVE :
			this->executeRemove(rt);
			break;
		case UPDATE :
			this->executeUpdate(rt);
			break;
		case CONFIG :
			this->executeConfig(rt);
			break;
		case PACKAGE :
			this->executePackage(rt);
			break;
		case EXTRACT :
			this->executeExtract(rt);
			break;
		case NEWDB :
			this->executeNewDb(rt);
			break;
		case QUERY :
			this->executeQuery(rt);
			break;
		case HELP :
			this->executeHelp();
			break;
		default :
			break;
	}

	return RETURN_SUCCESS;
}
