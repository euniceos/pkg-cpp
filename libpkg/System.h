/*
 * System.h
 *
 *  Created on: 15/03/2015
 *      Author: thebeast
 */

#include <cstring>
#include <vector>

#include "LibPkg.h"

#ifndef LIBPKG_SYSTEM_H_
#define LIBPKG_SYSTEM_H_

class System
{
public :
	System();
	arch_t systemArch;
	std::string packagesArch;
	std::vector<std::string> validPackagesArchs;

	ushort_t foundSystemArch(void);
	ushort_t foundPackagesArch(void);
	ushort_t foundValidArchs(void);
};

#endif /* LIBPKG_SYSTEM_H_ */
