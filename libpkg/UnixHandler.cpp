/*
 * UnixHandler.cpp
 *
 *  Created on: 15/03/2015
 *      Author: thebeast
 */


#include <cstdlib>
#include <cstring>

#if defined(__linux__)
#include <linux/limits.h>
#endif

#include "LibPkg.h"
#include "UnixHandlers.h"


UnixHandler::UnixHandler(std::string rawpath)
{
	rawPath = rawpath;
	fullPath = foundFullPath(rawPath);
	parentDir = foundParentDir(rawPath);
	fileName = foundFileName(rawPath);
}

std::string UnixHandler::foundFullPath(std::string rawpath)
{
	char buf[PATH_MAX +1];
	char* ptr;

	if((ptr = realpath(rawpath.c_str(), buf)) != NULL)
		return (std::string)ptr;

	return EMPTY;
}

std::string  UnixHandler::foundParentDir(std::string fullpath)
{

	return "HELLO";
}

std::string UnixHandler::foundFileName(std::string fullpath)
{

	return "HELLO";
}


