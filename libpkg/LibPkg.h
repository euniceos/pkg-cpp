/*
 * libpkg.h
 *
 *  Created on: 17/02/2015
 *      Author: miguel
 */

#include <iostream>
#include <string>
#include <vector>

#include <libintl.h>

#ifndef _
#define _(String) gettext (String)
#endif

#ifndef LIBSPKG_LIBSPKG_H_
#define LIBSPKG_LIBSPKG_H_


typedef unsigned short int ushort_t;
typedef signed short int sshort_t;

#define RETURN_FAILURE		0
#define	RETURN_SUCCESS		1
#define REQUIRED			1
#define NOREQUIRED			0
#define EMPTY				"empty"
#define FULL				"full"

typedef enum BOLEAN
{
	FALSE,
	TRUE
}bolean_t;

typedef enum ARCHS
{
	NODEF,
	BIT32,
	BIT64
}arch_t;

typedef enum ERROR
{
	FILE_NOT_FOUND,
	DIR_NOT_FOUND,
	DIR_NON_EMPTY,
	DIR_IS_EMPTY
} error_t;

#endif /* LIBSPKG_LIBSPKG_H_ */
