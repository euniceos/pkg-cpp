/*
 * UnixHandlers.h
 *
 *  Created on: 15/03/2015
 *      Author: thebeast
 */


#include <cstring>

#include "LibPkg.h"

#ifndef LIBPKG_UNIXHANDLERS_H_
#define LIBPKG_UNIXHANDLERS_H_

class UnixHandler
{
public :
	UnixHandler(std::string rawpath);
	std::string rawPath;
	std::string fullPath;
	std::string parentDir;
	std::string fileName;

protected :
	std::string foundFullPath(std::string rawpath);
	std::string foundParentDir(std::string fullpath);
	std::string foundFileName(std::string fullpath);
};

#endif /* LIBPKG_UNIXHANDLERS_H_ */
