/*
 * System.cpp
 *
 *  Created on: 05/03/2015
 *      Author: thebeast
 */


#include <cstdlib>
#include "System.h"
#include "LibPkg.h"

System::System()
{
	systemArch = NODEF;
}

ushort_t System::foundSystemArch(void)
{
	switch(__WORDSIZE)
	{
		case 32 :
			this->systemArch = BIT32;
			break;
		case 64 :
			this->systemArch = BIT64;
			break;
	}

	if(this->systemArch == NODEF)
		return RETURN_FAILURE;

	return RETURN_SUCCESS;
}

ushort_t System::foundPackagesArch(void)
{
	switch(this->systemArch)
	{
		case BIT32 :
			this->packagesArch = "i686";
			break;
		case BIT64 :
			this->packagesArch = "x86_64";
			break;
		default :
			break;
	}

	if(this->packagesArch == "none")
		return RETURN_FAILURE;

	return RETURN_SUCCESS;
}

ushort_t System::foundValidArchs(void)
{
	this->validPackagesArchs.clear();

	this->validPackagesArchs.push_back(packagesArch);
	if( this->systemArch == BIT64)
		this->validPackagesArchs.push_back("i686");
	this->validPackagesArchs.push_back("noarch");

	if(this->validPackagesArchs.size() == 0)
		return RETURN_FAILURE;

	return RETURN_SUCCESS;
}
